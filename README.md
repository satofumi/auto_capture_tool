# 自動スクショ取得ツール

## 目的

 - スクショを取ってファイル名を入力して保存するという非人道的な単純作業を撲滅する。


## 特徴

 - 対象がアプリが起動していたら自動で表示を切り替えさせつつスクショを取る。
 - 「クリック、キャプチャ、ファイル名をつけて保存」の動作を設定ファイルに記述できる。
 - 設定ファイルは yaml フォーマットで記述する。


## ライセンス

 - MIT ライセンス


## アイコンリソース

  - FatCow
    - Creative Commons Attribution 3.0 License
    - http://www.fatcow.com/free-icons


## Author
  - Satofumi
    - Twitter: @satofumi_


## 使い方

 - setting.yaml ファイルを編集する。
 - 対象のアプリが起動した状態で Capture ボタンを押す。


## setting.yaml の記述方法

```
output: '%Desktop%\Captures'
processes:
  process_name_0:
    output: 'output_folder_0'
    captures:
      - [20x20, 'first_tab.png', 100]
      - [120x20, 'second_tab.png']
      - [220x20, 'third_tab.png', 200]

  process_name_1:
    output: 'output_folder_1'
    captures:
      - [20x20, 'first_tab.png', 100]
      - [120x20, 'second_tab.png']
``` 

### この設定での動作

 - 各プロセスの出力フォルダはデスクトップの Captures フォルダ
 - process_name_0.exe か process_name_1.exe が実行されていればキャプチャを行う。
   - process_name_0.exe のキャプチャについて
     - キャプチャ画像は output_folder_0 というフォルダに保存される。
     - process_name_0.exe アプリウィンドウの左上から 20x20 の相対位置を左クリックして 100 ms 後に process_name_0.exe ウィンドウのキャプチャ画像を first_tab.png というファイル名で保存する。
     - キャプチャまでのミリ秒が省略された場合は、直前の設定値を利用する。（デフォルトは 0 ms）
     - 保存ファイル名の拡張子が '.png' の場合は PNG 画像として保存する。それ以外は BMP 画像で保存する。
