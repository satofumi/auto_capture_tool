VERSION = 1.0.0
PACKAGE_DIR = AutoCaptureTool-$(VERSION)

SLN_FILE = AutoCaptureTool.sln
MSBUILD = /c/Program\ Files\ \(x86\)/Microsoft\ Visual\ Studio/2017/Community/MSBuild/15.0/Bin/MSBuild.exe

BIN_DIR = AutoCaptureTool/bin/Release
DIST_FILES = $(BIN_DIR)/AutoCaptureTool.exe $(BIN_DIR)/*.dll $(BIN_DIR)/*.exe.config README.md LICENSE.txt ChangeLog.txt


all :

clean :
	$(RM) -rf $(PACKAGE_DIR)

dist : clean
	mkdir -p $(PACKAGE_DIR)
	$(MSBUILD) $(SLN_FILE) -t:build -p:Configuration=Release
	cp $(DIST_FILES) $(PACKAGE_DIR)
	zip -r $(PACKAGE_DIR).zip $(PACKAGE_DIR)
