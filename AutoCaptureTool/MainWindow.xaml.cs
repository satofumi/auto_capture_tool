﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;

namespace AutoCaptureTool
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        [StructLayout(LayoutKind.Sequential)]
        private struct POINT
        {
            public int x;
            public int y;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct RECT
        {
            public int Left;
            public int Top;
            public int Right;
            public int Bottom;
        }

        [DllImport("user32.dll")]
        static extern bool GetCursorPos(out POINT lpPoint);

        [DllImport("user32.dll", CallingConvention = CallingConvention.StdCall)]
        static extern void SetCursorPos(int x, int y);

        [DllImport("user32.dll", CallingConvention = CallingConvention.StdCall)]
        static extern void mouse_event(int dwFlags, int dx, int dy, int cButtons, int dwExtraInfo);

        private const int MOUSEEVENTF_LEFTDOWN = 0x2;
        private const int MOUSEEVENTF_LEFTUP = 0x4;
        private const int MOUSEEVENTF_ABSOLUTE = 0x8000;

        [DllImport("user32.dll")]
        static extern bool ClientToScreen(IntPtr hWnd, ref POINT lpPoint);

        [DllImport("user32.dll")]
        private static extern bool GetWindowRect(IntPtr hWnd, out RECT rect);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool SetForegroundWindow(IntPtr hWnd);


        private enum ProcessState
        {
            NotDetected = 0,
            Detected = 1,
        }

        private CaptureSetting captureSetting = new CaptureSetting();
        private BackgroundWorker processDetector = new BackgroundWorker();
        private string targetProcessName = "";


        public MainWindow()
        {
            InitializeComponent();

            // 設定ファイルを読み込む。
            LoadSettingFile();

            // 対象プロセスの検出処理を開始する。
            StartTargetProcessDetection();
        }

        private void LoadSettingFile()
        {
            try
            {
                string appDataSettingFile = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "AutoCaptureTool", "setting.yaml");
                string filePath = System.IO.File.Exists(appDataSettingFile) ? appDataSettingFile : "setting.yaml";
                captureSetting.Load(filePath);
            }
            catch (System.IO.FileNotFoundException)
            {
                // 何もしない。
            }
        }

        private void StartTargetProcessDetection()
        {
            processDetector.DoWork += ProcessDetector_DoWork;
            processDetector.ProgressChanged += ProcessDetector_ProgressChanged;
            processDetector.WorkerReportsProgress = true;
            processDetector.RunWorkerAsync();
        }

        private void ProcessDetector_DoWork(object sender, DoWorkEventArgs e)
        {
            while (true)
            {
                System.Threading.Thread.Sleep(10);

                // プロセス一覧を取得してマッチングを行う。
                var processes = ProcessNames(targetProcessName);
                var existsProcesses = captureSetting.ExistsProcesses(processes.Keys.ToList());
                if (existsProcesses.Count == 0)
                {
                    processDetector.ReportProgress((int)ProcessState.NotDetected);
                    continue;
                }

                foreach (string processName in existsProcesses)
                {
                    var handle = processes[processName];
                    if (handle == null)
                    {
                        continue;
                    }

                    // マッチしたプロセスがあればボタンを押せるようにし、カーソルの相対位置を表示する。
                    processDetector.ReportProgress((int)ProcessState.Detected, new KeyValuePair<string, IntPtr>(processName, handle));

                    // １つのプロセスのみを処理する。
                    break;
                }

                System.Threading.Thread.Sleep(1);
            }
        }

        private Dictionary<string, IntPtr> ProcessNames(string processName = null)
        {
            var names = new Dictionary<string, IntPtr>();

            var processes = string.IsNullOrWhiteSpace(processName) ? System.Diagnostics.Process.GetProcesses() : System.Diagnostics.Process.GetProcessesByName(processName);
            foreach (var process in processes)
            {
                try
                {
                    if (!names.ContainsKey(process.ProcessName))
                    {
                        names.Add(process.ProcessName, process.MainWindowHandle);
                    }
                }
                catch (Exception)
                {
                    // 何もしない。
                }
            }
            return names;
        }

        private void ProcessDetector_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {

            switch (e.ProgressPercentage)
            {
                case (int)ProcessState.NotDetected:
                    targetProcessName = "";
                    MessageLabel.Content = "The target application has not been detected.";
                    PositionLabel.Visibility = Visibility.Hidden;
                    CaptureButton.IsEnabled = false;
                    break;

                case (int)ProcessState.Detected:
                    var item = (KeyValuePair<string, IntPtr>)e.UserState;

                    targetProcessName = item.Key;
                    var handle = item.Value;

                    MessageLabel.Content = targetProcessName;

                    var position = WindowLeftTop(handle);
                    var cursor = CursorPosition();
                    PositionLabel.Content = "(" + (cursor.x - position.x).ToString() + ", " + (cursor.y - position.y).ToString() + ")";

                    CaptureButton.IsEnabled = true;
                    break;
            }
        }

        private POINT WindowLeftTop(IntPtr handle)
        {
            var position = new POINT();
            ClientToScreen(handle, ref position);
            return position;
        }

        private POINT CursorPosition()
        {
            var position = new POINT();
            GetCursorPos(out position);
            return position;
        }

        private void CaptureButton_Click(object sender, RoutedEventArgs e)
        {
            CaptureImages();
        }

        private void CaptureImages()
        {
            var setting = captureSetting.ProcessSetting(targetProcessName);
            string outputFolder = System.IO.Path.Combine(captureSetting.BaseOutputFolder, setting.OutputFolder);
            System.IO.Directory.CreateDirectory(outputFolder);

            // キャプチャ開始時のカーソル位置を保存しておいてキャプチャ後に復帰させる。
            var cursor = CursorPosition();

            foreach (var item in ProcessNames(targetProcessName))
            {
                foreach (var capture in setting.Captures)
                {
                    // プロセスのウィンドウ位置を取得する。
                    var handle = item.Value;
                    var position = WindowLeftTop(handle);

                    // ウィンドウをアクティブにする。
                    SetForegroundWindow(handle);

                    // 指定位置をクリックさせる。
                    var clickPosition = new System.Windows.Point(position.x + capture.ClickPosition.X, position.y + capture.ClickPosition.Y);
                    SetCursorPos((int)clickPosition.X, (int)clickPosition.Y);
                    mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0);
                    mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);

                    // 指定ミリ秒だけ待つ。
                    System.Threading.Thread.Sleep(capture.DelayMsec);

                    // 画像をキャプチャして保存する。
                    string filePath = System.IO.Path.Combine(outputFolder, capture.OutputName);
                    SaveCaptureImage(filePath, handle);
                }

                // １つのプロセスのみを処理する。
                break;
            }

            // カーソル位置を元に戻す。
            SetCursorPos(cursor.x, cursor.y);

            // 保存先フォルダを開く。
            Process.Start(outputFolder);
        }

        private void SaveCaptureImage(string filePath, IntPtr handle)
        {
            var rect = new RECT();
            GetWindowRect(handle, out rect);

            int width = rect.Right - rect.Left;
            int height = rect.Bottom - rect.Top;
            var bitmap = new Bitmap(width, height);
            using (var g = Graphics.FromImage(bitmap))
            {
                g.CopyFromScreen(rect.Left, rect.Top, 0, 0, bitmap.Size);
            }

            var format = ImageFormat.Bmp;
            switch (System.IO.Path.GetExtension(filePath).ToLower())
            {
                case ".png":
                    format = ImageFormat.Png;
                    break;
            }

            bitmap.Save(filePath, format);
        }
    }
}
