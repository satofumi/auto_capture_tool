﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using YamlDotNet.RepresentationModel;

namespace AutoCaptureTool
{
    public class CaptureSetting
    {
        private class YamlSetting
        {
            public class ProcessSetting
            {
                public string Output { set; get; }
                public List<List<string>> Captures { set; get; }
            }

            public string Output { set; get; }
            public Dictionary<string, ProcessSetting> Processes { set; get; }
        }


        public class Capture
        {
            public Point ClickPosition { set; get; }
            public string OutputName { set; get; }
            public int DelayMsec { set; get; }
        }

        public class Setting
        {
            public string OutputFolder { set; get; }
            public List<Capture> Captures { set; get; }

            public Setting()
            {
                Captures = new List<Capture>();
            }
        }


        private string baseOutputFolder = "";
        private Dictionary<string, Setting> captureSettings = new Dictionary<string, Setting>();


        public void Load(string filePath)
        {
            captureSettings.Clear();

            var loadSettings = YamlImporter.Deserialize<YamlSetting>(filePath);
            foreach (var process in loadSettings.Processes)
            {
                var setting = new Setting();
                string processName = process.Key;
                setting.OutputFolder = process.Value.Output;

                int delayMsec = 0;
                foreach (var captureLine in process.Value.Captures)
                {
                    var capture = new Capture();
                    capture.ClickPosition = ParsePoint(captureLine[0]);
                    capture.OutputName = captureLine[1];
                    if (captureLine.Count >= 3)
                    {
                        delayMsec = int.Parse(captureLine[2]);
                    }
                    capture.DelayMsec = delayMsec;
                    setting.Captures.Add(capture);
                }

                captureSettings.Add(processName, setting);
            }

            baseOutputFolder = ReplaceSpecialPath(loadSettings.Output);
        }

        private Point ParsePoint(string token)
        {
            var tokens = token.Trim().Split('x');
            return new Point(double.Parse(tokens[0]), double.Parse(tokens[1]));
        }

        private string ReplaceSpecialPath(string path)
        {
            var tokens = path.Split('\\');
            var replaced = new List<string>();
            foreach (var token in tokens)
            {
                if (token == "%Desktop%")
                {
                    replaced.Add(Environment.GetFolderPath(Environment.SpecialFolder.Desktop));
                }
                else
                {
                    replaced.Add(token);
                }
            }

            return Path.Combine(replaced.ToArray());
        }

        public List<string> ExistsProcesses(List<string> names)
        {
            return names.Intersect(new List<string>(captureSettings.Keys)).ToList();
        }

        public Setting ProcessSetting(string processName)
        {
            return captureSettings[processName];
        }

        public string BaseOutputFolder
        {
            get
            {
                return baseOutputFolder;
            }
        }
    }
}
