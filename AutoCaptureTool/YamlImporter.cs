﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

namespace AutoCaptureTool
{
    public class YamlImporter
    {
        public static T Deserialize<T>(string filePath)
        {
            var reader = new StreamReader(filePath);
            string text = reader.ReadToEnd();
            var input = new StringReader(text);
            var deserializer = new DeserializerBuilder().WithNamingConvention(new UnderscoredNamingConvention()).Build();
            return deserializer.Deserialize<T>(input);
        }

        public static Point ParseSize(string sizeText)
        {
            var tokens = sizeText.Trim().Split('x');
            return new Point(float.Parse(tokens[0]), float.Parse(tokens[1]));
        }
    }
}